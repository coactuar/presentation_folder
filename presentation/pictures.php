	<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

<style>
a {
color: white;
font-size: 20px;
}
</style>

<style>
body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
  display: none;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>


</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-12 text-right">
            
        </div>
    </div>
    <div class="row video-panel">
	
			
	
	
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/BON K2.RGB_color.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
            </div>
            <h3>Image 1</h3>    
        </div>
       <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/EXHIBITION HALL.RGB_color.jpg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
            </div>
            <h3>Image 2</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/IMG-20200708-WA0015.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
            </div>
            <h3>Image 3</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/LOBBY_25_07_2020.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
            </div>
            <h3>Image 4</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-07-23 at 11.55.42 AM.jpeg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(5)" class="hover-shadow cursor" >
            </div>
            <h3>Image 5</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-07-25 at 6.23.58 PM.jpeg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
            </div>
            <h3>Image 6</h3>    
			</div>
			<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-11-03 at 4.34.16 PM.jpeg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(7)" class="hover-shadow cursor" >
            </div>
            <h3>Image 7</h3>    
			</div>
			<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-11-03 at 10.51.09 PM.jpeg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
            </div>
            <h3>Image 8</h3>    
			</div>
			<!--
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-11-04 at 10.47.15 AM.jpeg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(9)" class="hover-shadow cursor" >
            </div>
            <h3>Image 9</h3>    
        </div>
		-->

		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-11-04 at 10.55.19 AM.jpeg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(9)" class="hover-shadow cursor" >
            </div>
            <h3>Image 9</h3>    
        </div>
		<!--
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <img src="data/WhatsApp Image 2020-11-04 at 11.36.54 AM.jpeg" class="embed-responsive-item" width=100% onclick="openModal();currentSlide(11)" class="hover-shadow cursor" >
            </div>
            <h3>Image 11</h3>    
        </div>
		-->
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (1).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
            </div>
            <h3>Image 10</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (2).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(11)" class="hover-shadow cursor">
            </div>
            <h3>Image 11</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (3).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(12)" class="hover-shadow cursor">
            </div>
            <h3>Image 12</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (4).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(13)" class="hover-shadow cursor">
            </div>
            <h3>Image 13</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (5).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(14)" class="hover-shadow cursor">
            </div>
            <h3>Image 14</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (6).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(15)" class="hover-shadow cursor">
            </div>
            <h3>Image 15</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (7).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(16)" class="hover-shadow cursor">
            </div>
            <h3>Image 16</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (8).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(17)" class="hover-shadow cursor">
            </div>
            <h3>Image 17<h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (9).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(18)" class="hover-shadow cursor">
            </div>
            <h3>Image 18</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (10).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(19)" class="hover-shadow cursor">
            </div>
            <h3>Image 19</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (11).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(20)" class="hover-shadow cursor">
            </div>
            <h3>Image 20</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/new1 (12).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(21)" class="hover-shadow cursor">
            </div>
            <h3>Image 21</h3>    
        </div>
    
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/stall1.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(22)" class="hover-shadow cursor">
            </div>
            <h3>Image 22</h3>    
      </div>
     <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/stall2.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(23)" class="hover-shadow cursor">
            </div>
            <h3>Image 23</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/stall3.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(24)" class="hover-shadow cursor">
            </div>
            <h3>Image 24</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/stall4.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(25)" class="hover-shadow cursor">
            </div>
            <h3>Image 25</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/stall5.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(26)" class="hover-shadow cursor">
            </div>
            <h3>Image 26</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Timeline Hi Res_01.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(27)" class="hover-shadow cursor">
            </div>
            <h3>Image 27</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/welcome.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(28)" class="hover-shadow cursor">
            </div>
            <h3>Image 28</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/engagement.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(29)" class="hover-shadow cursor">
            </div>
            <h3>Image 29</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/EXHIBITION HALL_.effectsResult (1).jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(30)" class="hover-shadow cursor">
            </div>
            <h3>Image 30</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/innovationlab.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(31)" class="hover-shadow cursor">
            </div>
            <h3>Image 31</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Intergrace auditorium frame A.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(32)" class="hover-shadow cursor">
            </div>
            <h3>Image 32</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/lobby.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(33)" class="hover-shadow cursor">
            </div>
            <h3>Image 33</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/lobby.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(34)" class="hover-shadow cursor">
            </div>
            <h3>Image 34</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/lounge.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(35)" class="hover-shadow cursor">
            </div>
            <h3>Image 35</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Dubinor Oinment.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(36)" class="hover-shadow cursor">
            </div>
            <h3>Image 36</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Dubinor Tablets.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(37)" class="hover-shadow cursor">
            </div>
            <h3>Image 37</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Ebov.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(38)" class="hover-shadow cursor">
            </div>
            <h3>Image 38</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Esoz.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(39)" class="hover-shadow cursor">
            </div>
            <h3>Image 39</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Lizolid.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(40)" class="hover-shadow cursor">
            </div>
            <h3>Image 40</h3>    
        </div><div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Stiloz.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(41)" class="hover-shadow cursor">
            </div>
            <h3>Image 41</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Stiloz.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(42)" class="hover-shadow cursor">
            </div>
            <h3>Image 42</h3>    
        </div> 
      <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Vconnent.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(42)" class="hover-shadow cursor">
            </div>
            <h3>Image 42</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/agenda.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(43)" class="hover-shadow cursor">
            </div>
            <h3>Image 43</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/bg.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(44)" class="hover-shadow cursor">
            </div>
            <h3>Image 44</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/boardroom.jpg" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(45)" class="hover-shadow cursor">
            </div>
            <h3>Image 45</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Bandhan.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(46)" class="hover-shadow cursor">
            </div>
            <h3>Image 46</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of BonDK.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(47)" class="hover-shadow cursor">
            </div>
            <h3>Image 47</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of BonK2.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(48)" class="hover-shadow cursor">
            </div>
            <h3>Image 48</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <img src="data/Copy of Collasmart - A.png" class="embed-responsive-item" width=100%  onclick="openModal();currentSlide(49)" class="hover-shadow cursor">
            </div>
            <h3>Image 49</h3>    
        </div>
    </div> 
	
	<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 49</div>
      <img src="data/BON K2.RGB_color.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 49</div>
      <img src="data/EXHIBITION HALL.RGB_color.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 49</div>
      <img src="data/IMG-20200708-WA0015.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 49</div>
      <img src="data/LOBBY_25_07_2020.jpg" style="width:100%">
    </div>
	
	<div class="mySlides">
      <div class="numbertext">5 / 49</div>
      <img src="data/WhatsApp Image 2020-07-23 at 11.55.42 AM.jpeg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">6 / 49</div>
      <img src="data/WhatsApp Image 2020-07-25 at 6.23.58 PM.jpeg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">7 / 49</div>
      <img src="data/WhatsApp Image 2020-11-03 at 4.34.16 PM.jpeg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">8 / 49</div>
      <img src="data/WhatsApp Image 2020-11-03 at 10.49.09 PM.jpeg" style="width:100%">
    </div>
	<!--
	<div class="mySlides">
      <div class="numbertext">9 / 23</div>
      <img src="data/WhatsApp Image 2020-11-04 at 10.47.15 AM.jpeg" style="width:100%">
    </div>
	-->
	<div class="mySlides">
      <div class="numbertext">9 / 49</div>
      <img src="data/WhatsApp Image 2020-11-04 at 10.55.19 AM.jpeg" style="width:100%">
    </div>
	<!--
	<div class="mySlides">
      <div class="numbertext">11 / 23</div>
      <img src="data/WhatsApp Image 2020-11-04 at 11.36.54 AM.jpeg" style="width:100%">
    </div>
	-->
	<div class="mySlides">
      <div class="numbertext">10 / 49</div>
      <img src="data/new1 (1).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">11 / 49</div>
      <img src="data/new1 (2).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">12 / 49</div>
      <img src="data/new1 (3).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">13 / 49</div>
      <img src="data/new1 (4).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">14 / 49</div>
      <img src="data/new1 (5).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">15 / 49</div>
      <img src="data/new1 (6).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">16 / 49</div>
      <img src="data/new1 (7).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">17 / 49</div>
      <img src="data/new1 (8).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">18 / 49</div>
      <img src="data/new1 (9).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">19 / 49</div>
      <img src="data/new1 (10).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">20 / 49</div>
      <img src="data/new1 (11).jpg" style="width:100%">
    </div>
	<div class="mySlides">
      <div class="numbertext">21 / 49</div>
      <img src="data/new1 (12).jpg" style="width:100%">
    </div>
   <div class="mySlides">
      <div class="numbertext">22 / 49</div>
      <img src="data/stall1.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">23 / 49</div>
      <img src="data/stall2.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">24 / 49</div>
      <img src="data/stall3.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">25 / 49</div>
      <img src="data/stall4.jpg" style="width:100%">
    </div>
       <div class="mySlides">
      <div class="numbertext">26 / 49</div>
      <img src="data/stall5.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">27 / 49</div>
      <img src="data/Timeline Hi Res_01.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">28 / 49</div>
      <img src="data/welcome.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">29 / 49</div>
      <img src="data/engagement.png" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">30 / 49</div>
      <img src="data/EXHIBITION HALL_.effectsResult (1).jpg" style="width:100%">
    </div>
   
    <div class="mySlides">
      <div class="numbertext">31 / 49</div>
      <img src="data/innovationlab.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">32 / 49</div>
      <img src="data/Intergrace auditorium frame A.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">33 / 49</div>
      <img src="data/lobby.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">34 / 49</div>
      <img src="data/lobby.png" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">35 / 49</div>
      <img src="data/lounge.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">36 / 49</div>
      <img src="data/Copy of Dubinor Oinment.jpg" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">37 / 49</div>
      <img src="data/Copy of Dubinor Tablets.png" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">38 / 49</div>
      <img src="data/Copy of Ebov.png" style="width:100%">
    </div>
     <div class="mySlides">
      <div class="numbertext">39 / 49</div>
      <img src="data/Copy of Esoz.png" style="width:100%">
    </div><div class="mySlides">
      <div class="numbertext">40 / 49</div>
      <img src="data/Copy of Lizolid.png" style="width:100%">
    </div>
     <div class="mySlides">
      <div class="numbertext">41 / 49</div>
      <img src="data/Copy of Stiloz.png" style="width:100%">
    
    </div>
     <div class="mySlides">
      <div class="numbertext">42 / 49</div>
      <img src="data/Copy of Vconnent.png" style="width:100%">
    </div>  
    
     <div class="mySlides">
      <div class="numbertext">43 / 49</div>
      <img src="data/agenda.jpg" style="width:100%">
    </div>   <div class="mySlides">
      <div class="numbertext">44 / 49</div>
      <img src="data/bg.jpg" style="width:100%">
    </div>   
    <div class="mySlides">
      <div class="numbertext">45 / 49</div>
      <img src="data/boardroom.jpg" style="width:100%">
    </div>   
    
     <div class="mySlides">
      <div class="numbertext">46 / 49</div>
      <img src="data/Copy of Bandhan.png" style="width:100%">
    </div>   <div class="mySlides">
      <div class="numbertext">47 / 49</div>
      <img src="data/Copy of BonDK.png" style="width:100%">
    </div>  <div class="mySlides">
      <div class="numbertext">48 / 49</div>
      <img src="data/Copy of BonK2.png" style="width:100%">
    </div>   <div class="mySlides">
      <div class="numbertext">49 / 49</div>
      <img src="data/Copy of Collasmart - A.png" style="width:100%">
    </div>   
    <!-- <div class="mySlides">
      <div class="numbertext">50 / 51</div>
      <img src="data/new1 (12).jpg" style="width:100%">
    </div>   </div><div class="mySlides">
      <div class="numbertext">51 / 51</div>
      <img src="data/new1 (12).jpg" style="width:100%"> -->
    </div>
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>

<!--
    <div class="column">
      <img class="demo cursor" src="data/BON K2.RGB_color.jpg" style="width:100%" onclick="currentSlide(1)" alt="Image 1">
    </div>
    <div class="column">
      <img class="demo cursor" src="data/EXHIBITION HALL.RGB_color.jpg" style="width:100%" onclick="currentSlide(2)" alt="Image 2">
    </div>
    <div class="column">
      <img class="demo cursor" src="data/IMG-20200708-WA0015.jpg" style="width:100%" onclick="currentSlide(3)" alt="Image 3">
    </div>
    <div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-07-23 at 11.55.42 AM.jpeg" style="width:100%" onclick="currentSlide(4)" alt="Image 4">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-07-25 at 6.23.58 PM.jpeg" style="width:100%" onclick="currentSlide(5)" alt="Image 5">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-03 at 4.34.16 PM.jpeg" style="width:100%" onclick="currentSlide(6)" alt="Image 6">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-03 at 10.51.09 PM.jpeg" style="width:100%" onclick="currentSlide(7)" alt="Image 7">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-04 at 10.47.15 AM.jpeg" style="width:100%" onclick="currentSlide(8)" alt="Image 8">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-04 at 10.55.19 AM.jpeg" style="width:100%" onclick="currentSlide(9)" alt="Image 9">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-04 at 10.55.19 AM.jpeg" style="width:100%" onclick="currentSlide(10)" alt="Image 10">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/WhatsApp Image 2020-11-04 at 11.36.54 AM.jpeg" style="width:100%" onclick="currentSlide(11)" alt="Image 11">
    </div>
	
	<div class="column">
      <img class="demo cursor" src="data/new1 (1).jpg" style="width:100%" onclick="currentSlide(12)" alt="Image 12">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/data/new1 (2).jpg" style="width:100%" onclick="currentSlide(13)" alt="Image 13">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (3).jpg" style="width:100%" onclick="currentSlide(14)" alt="Image 14">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (4).jpg" style="width:100%" onclick="currentSlide(15)" alt="Image 15">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (5).jpg" style="width:100%" onclick="currentSlide(16)" alt="Image 16">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (6).jpg" style="width:100%" onclick="currentSlide(17)" alt="Image 17">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (7).jpg" style="width:100%" onclick="currentSlide(18)" alt="Image 18">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (8).jpg" style="width:100%" onclick="currentSlide(19)" alt="Image 19">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (9).jpg" style="width:100%" onclick="currentSlide(20)" alt="Image 20">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (10).jpg" style="width:100%" onclick="currentSlide(21)" alt="Image 21">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (11).jpg" style="width:100%" onclick="currentSlide(22)" alt="Image 22">
    </div>
	<div class="column">
      <img class="demo cursor" src="data/new1 (12).jpg" style="width:100%" onclick="currentSlide(23)" alt="Image 23">
    </div>
	-->
  </div>
</div>
	
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
//showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  //dots[slideIndex-1].className += " active";
  //captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>


</body>
</html>