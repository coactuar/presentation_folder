<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
a {
color: white;
font-size: 20px;
}
</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-12 text-right">
            
        </div>
    </div>
    <div class="row video-panel">
        
        <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/VID-20200728-WA0031.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 1</h3>   
        </div>
        <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
				<video class="embed-responsive-item" controls>
					  <source src="data/zoom_0.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
            <h3>VIDEO 2</h3>    
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Lobby Animation_01.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 3</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/VID-20201013-WA0002.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 4</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/VID-20200916-WA0000.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 5</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Virtual Event Coact.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 6</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Virtual Webinar Solution.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 7</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Virtual bg with multi layouts.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 8</h3>   
        </div>
		<div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/New Project -Marquee-ThysunKrupp Virtual.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 9</h3>   
        </div>
        
        <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Capgemini Lobby Camera 01.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 10</h3>   
        </div>
        <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Capgemini Lobby Camera 02.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 11</h3>   
        </div>
    
    <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Integrace City Tour ( Revised Integrace Logo on HO ).mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 12</h3>   
        </div>
    </div>
    <div class="col-12 col-lg-6 text-center">
            <div class="embed-responsive embed-responsive-16by9">
             <video class="embed-responsive-item" controls>
					  <source src="data/Lobby Walkthrough.mp4" type="video/mp4">
					
						Your browser does not support the video tag.
				</video>
            </div>    
             <h3>VIDEO 13</h3>   
        </div>
    </div>

</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>