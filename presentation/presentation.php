<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
a {
color: white;
font-size: 20px;
}
</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-12 text-right">
            
        </div>
    </div>
	
	 
	
    <div class="row video-panel">
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9" style="list-style-type: none;top: 30%;">
				<div class="embed-responsive-item">
					<li><a href="data/presentation.pdf">Presentation 1</a></li>
					<!--<li><a href="data/TGS Virtual Platform.pdf">Presentation 2</a></li>-->
					<li><a href="data/COACT New Layout Samples.pdf">Presentation 2</a></li>
				</div>
            </div>
            <h3>PRESENTATION</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <a href="pictures.php" target="_blank"><img src="data/EXHIBITION HALL.RGB_color.jpg" class="embed-responsive-item" width=100%></a>
            </div>    
            <h3>ACTUAL IMAGES</h3>    
        </div>
        <div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9">
				<a href="video.php" target="_blank"><img src="data/video.PNG" class="embed-responsive-item" width=100% ></a>
            </div>    
            <h3>VIDEOS</h3>    
        </div>
		<div class="col-12 col-lg-3 text-center">
            <div class="embed-responsive embed-responsive-16by9" style="list-style-type: none;top: 30%;">
				<div class="embed-responsive-item">
					
					<li><a href="https://coact.live/webinar-demo/index.php">Webinar Platform</a></li>
					<li><a href="https://coact.live/productvideos/">Production Videos</a></li>
					
				</div>
            </div>    
            <h3>REFERENCE LINKS</h3>    
        </div>
        
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>