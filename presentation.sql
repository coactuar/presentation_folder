-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 02:37 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `presentation`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_emailid` varchar(500) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_phone`, `user_emailid`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'v', '3242', 'viraj@coact.co.in', '2020-11-30 17:29:10', '2021-07-27 12:26:15', '2021-07-27 12:26:45', 0, 'presentation'),
(2, 'Reynold', '09611518064', 'reynold@coact.co.in', '2020-11-30 17:36:17', '2021-05-10 11:39:41', '2021-05-10 11:40:11', 0, 'presentation'),
(3, 'Pooja', '0', 'pooja@coact.co.in', '2020-11-30 17:55:56', '2021-12-21 18:38:39', '2021-12-21 18:39:09', 0, 'presentation'),
(4, 'pawan', '9545329222', 'pawan@coact.co.in', '2020-11-30 18:15:13', '2021-09-07 17:28:34', '2021-09-07 17:29:04', 0, 'presentation'),
(5, 'priyanka', '0', 'priyanka@freedomfromdiabetes.org', '2020-11-30 18:36:20', '2020-11-30 18:36:20', '2020-11-30 18:36:50', 0, 'presentation'),
(6, 'Sujatha', '9845563760', 'sujatha@coact.co.in', '2020-12-01 13:23:47', '2021-06-24 11:55:37', '2021-06-24 11:56:07', 0, 'presentation'),
(7, 'Deepti Vaidyanathan', '0', 'deepti.vaidyanathan@allergan.com', '2020-12-01 14:40:37', '2020-12-01 14:40:37', '2020-12-01 14:41:07', 0, 'presentation'),
(8, 'Melvin George', '0', 'melvin.george@biocon.com', '2020-12-01 14:50:08', '2020-12-01 14:50:08', '2020-12-01 14:50:38', 0, 'presentation'),
(9, 'Manick', '0', 'manickavasagam.sundaram@altran.com', '2020-12-01 15:00:44', '2020-12-01 15:00:44', '2020-12-01 15:01:14', 0, 'presentation'),
(10, 'Anil kumar', '0', 'amangalgi@yahoo.com', '2020-12-01 16:16:49', '2020-12-01 16:16:49', '2020-12-01 16:17:19', 0, 'presentation'),
(31, 'Namrata', '0', 'namratashetty.9320@gmail.com', '2021-04-29 22:05:27', '2021-04-29 22:05:27', '2021-04-29 22:05:57', 0, 'presentation'),
(11, 't', '0', 't@siemens.com', '2020-12-02 14:42:06', '2020-12-02 14:42:06', '2020-12-02 14:42:36', 0, 'presentation'),
(12, 'Akshat Jharia', '7204420017', 'akshatjharia@gmail.com', '2020-12-04 15:44:03', '2021-09-28 20:09:02', '2021-09-28 20:09:32', 0, 'presentation'),
(13, 'Akshat Jharia', '07204420017', 'akshat@coact.co.in', '2020-12-04 15:44:17', '2020-12-04 15:44:17', '2020-12-04 15:44:47', 0, 'presentation'),
(14, 'Suvendu Roy', '9987522278', 'suvendu@titan.co.in', '2020-12-04 19:02:15', '2020-12-04 19:02:15', '2020-12-04 19:02:45', 0, 'presentation'),
(15, 'Saurabh', '08446998720', 'saurabhdpuranik@gmail.com', '2020-12-05 12:21:31', '2021-09-14 09:45:57', '2021-09-14 09:46:27', 0, 'presentation'),
(16, 'MOMITA DAS', '0', 'momita@titan.co.in', '2020-12-09 18:26:06', '2020-12-09 18:26:06', '2020-12-09 18:26:36', 0, 'presentation'),
(17, 'Padam Malik', '0', 'padam@sonalika.com', '2020-12-11 16:39:48', '2020-12-11 16:39:48', '2020-12-11 16:40:18', 0, 'presentation'),
(18, 'mahesh', '0', 'gromordigital1404@gmail.com', '2020-12-12 19:06:14', '2020-12-12 19:20:11', '2020-12-12 19:20:41', 0, 'presentation'),
(19, 'Nishanth', '0', 'nishanth.1911@gmail.com', '2020-12-13 20:56:23', '2020-12-24 11:13:26', '2020-12-24 11:13:56', 0, 'presentation'),
(20, 'Siddhu Raju', '9880302478', 'siddhur@titan.co.in', '2020-12-14 19:38:24', '2020-12-14 19:41:00', '2020-12-14 19:41:30', 0, 'presentation'),
(21, 'tanvi', '0', 'tanvi.hegde@siemsn.com', '2020-12-15 13:47:04', '2020-12-15 13:47:04', '2020-12-15 13:47:34', 0, 'presentation'),
(27, 'Tresa Wilson', '0', 'tresa.wilson@capgemini.com', '2021-02-08 10:42:49', '2021-02-08 10:42:49', '2021-02-08 10:43:19', 0, 'presentation'),
(22, 'Kalpesh Rathi', '09822206611', 'kalpesh@ecell.in', '2020-12-27 12:09:33', '2020-12-27 13:33:42', '2020-12-27 13:34:12', 0, 'presentation'),
(23, 'tanvi', '0', 'tan@gjem.com', '2020-12-29 13:50:09', '2020-12-29 13:50:09', '2020-12-29 13:50:39', 0, 'presentation'),
(24, 't', '0', 't@gm.com', '2020-12-30 16:08:39', '2020-12-30 16:08:39', '2020-12-30 16:09:09', 0, 'presentation'),
(25, 'MK', '0', 'ab@gmail.com', '2020-12-30 16:49:05', '2020-12-30 16:49:05', '2020-12-30 16:49:35', 0, 'presentation'),
(26, 'tanvi', '0', 't@gmail.com', '2021-01-06 09:50:56', '2021-01-06 09:50:56', '2021-01-06 09:51:26', 0, 'presentation'),
(28, 'V', '6546', 'test1@coact.co.in', '2021-03-04 17:05:42', '2021-04-09 10:01:56', '2021-04-09 10:02:26', 0, 'presentation'),
(29, 'Nikunj', '9820944924', 'nikunj@supergemsindia.com', '2021-03-12 16:33:58', '2021-03-19 10:40:09', '2021-03-19 10:40:39', 0, 'presentation'),
(30, 'Tanushree', '7795702662', 'tanushree@coact.co.in', '2021-03-24 09:56:57', '2021-04-27 13:29:07', '2021-04-27 13:29:37', 0, 'presentation'),
(32, 'neeraj', '08367773537', 'pandurevanthreddy002@gmail.com', '2021-05-13 10:44:54', '2021-05-14 17:07:34', '2021-05-14 17:08:04', 0, 'presentation'),
(33, 'Nishanth.S', '54', 'nishanth@coact.co.in', '2021-07-06 11:48:57', '2021-07-06 11:48:57', '2021-07-06 11:49:27', 0, 'presentation'),
(34, 'pooja', '0', 'pooja@gmail.com', '2021-07-30 10:53:52', '2021-07-30 10:53:52', '2021-07-30 10:54:22', 0, 'presentation'),
(35, 'pooja', '0', 'pooja@coact.com', '2021-08-17 18:48:50', '2021-08-17 18:48:50', '2021-08-17 18:49:20', 0, 'presentation'),
(36, 'Ambarish Joshi', '09011084867', 'ambarish.joshi@mitwpu.edu.in', '2021-08-20 11:58:04', '2021-09-17 15:25:01', '2021-09-17 15:25:31', 0, 'presentation'),
(37, '7361016101', '07361016101', 'biramaneatharva@gmail.com', '2021-09-02 11:30:11', '2021-09-02 17:01:48', '2021-09-02 17:02:18', 0, 'presentation'),
(38, 'Jagadish', '0', 'jagadish@coact.co.in', '2021-09-04 09:53:28', '2021-09-06 11:56:09', '2021-09-06 11:56:39', 0, 'presentation'),
(39, 'Pranav Vhanbatte', '07744094521', 'pssvhanbatte@gmail.com', '2021-09-07 14:10:01', '2021-09-07 14:41:13', '2021-09-07 14:41:43', 0, 'presentation'),
(40, 'Anushree Natesh', '9886587721', 'anushree99.n@gmail.com', '2021-09-15 15:20:00', '2021-09-15 15:20:00', '2021-09-15 15:20:30', 0, 'presentation'),
(41, 'pawan', '9545329222', 'pawan@coat.co.in', '2021-10-20 12:00:40', '2021-10-20 12:00:40', '2021-10-20 12:01:10', 0, 'presentation'),
(42, 'Aj', 'Ueueueue', 'aj@gmail.com', '2022-01-04 12:24:41', '2022-01-04 12:24:41', '2022-01-04 12:25:11', 0, 'presentation');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
